## Suryani_Vebhitry_Siahaan

{+ Git adalah sebuah alat atau sistem pengontrol versi yang digunakan developper untuk mengembangkan proyek dan memantau semua perubahan yang terjadi pada file proyek tanpa harus repot-repot membuat ulang atau menghapus file tersebut. Proyek tersebut dapat dikerjakan offline ataupun online.+}
Git ini sebenernya memudahkan programmer untuk mengetahui perubahan source codenya daripada harus membuat file baru seperti Program.java, ProgramRevisi.java, ProgramRevisi2.java, ProgramFix.java. Selain itu, dengan git kita tak perlu khawatir code yang kita kerjakan bentrok, karena setiap developer bias membuat branch sebagai workspacenya.Fitur yang tak kalah keren lagi, pada git kita bisa memberi komentar pada source code yang telah ditambah/diubah, hal ini mempermudah developer lain untuk tahu kendala apa yang dialami developer lain.


https://blog-aflasio.netlify.app/post/tutorial-dasar-git/
